var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var fs = require('fs');
var url = require('url');
var sql = require('mysql');
var session = require('express-session');

var pool = require('./bin/conn/index');

var router = express.Router();
var routes=require('./routes/index');
var content = require('./routes/content');
var add = require('./routes/add');


var app = express();

var config = require('./config.js');


// view engine setup
app.set('views', path.join(__dirname, 'views/pages'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public',   'favicon.png')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static(path.join(__dirname, 'public')));
app.use(session({secret:'ssshhhhh1',saveUninitialized: true,resave: true}));
app.use(bodyParser.urlencoded({extended: true}));
app.use('/form_handler', bodyParser.urlencoded({
	extended: true
}));

app.use('/', routes);
app.use('/content', content);
app.use('/add', add);




//This block code avaible wokr with ethereum blokchain
var Web3 = require("web3");
var web3 = new Web3();
web3.setProvider(new web3.providers.HttpProvider(config.EthereumHost));
//web3.eth.defaultAccount = web3.eth.coinbase;
web3.eth.defaultAccount = config.EthereumUserAccount;
web3.personal.unlockAccount(config.EthereumUserAccount, config.EthereumUserPassword);
//web3.personal.lockAccount(config.EthereumUserAccount, config.EthereumUserPassword);
var EthContract = web3.eth.contract(config.ConractAbi).at(config.ConractAdress);


function GetTransactionBlockChain(param,tryAgain) {
    if (tryAgain === undefined) {
        tryAgain = true;
    }


     try {
        var hash = web3.sha3(param);
        var idTransaction = EthContract.SetHash(hash, {gas: 300000});
        return {idTransaction,hash};

    } catch (e){

         console.log(e.message);

         if (e.message == "account is locked") {

             web3.personal.unlockAccount(config.EthereumUserAccount, config.EthereumUserPassword);

             if (tryAgain) {

                 return GetTransactionBlockChain(param, false);

             }
         }


         return  -1;

     };

}


app.post('/login', function (req,res,next) {
	pool.pool.query('select * from users where name = "'+req.body.user+'"',function (err,rows,field) {
		if(err) throw err;
		else if(rows.length > 0 && req.body.password == rows[0].password){
			req.session.user = rows[0].name;
            req.session.firstLog = 'ok';
			res.send('');
		}else{
			var wrongLogOrPass = 'wrongLogOrPass';
			res.send(wrongLogOrPass);
		}
	});

});

app.post('/logout',function (req,res) {
	delete req.session.user;
	res.send('');
});


app.post('/addNewFirm',function (req,res,next) {
	if(!req.session.user){
	    res.send('You should login first');
    } else {
        var CurrentDate = new Date();

        // call the contract
        var param = req.session.user+"#"+req.body.firm+"#"+req.body.ChekBoxCleared+"#"+req.body.ChekBoxTransacted;
        var res_struct = GetTransactionBlockChain(param);
        if (res_struct ==-1) {
            res.send("Ethereum TestNET is unavailable, please try again in couple of minutes");
            return;
        }

		pool.pool.query('select * from firms where name ="'+ req.body.newFirm +'"', function (err, rows, field) {
			if (rows[0]) {
				res.send('The Firm already exists');
			}
			else {
                req.session.currentCounterparty = req.body.newFirm;
				var name = req.body.newFirm;


                pool.pool.query('insert into firms set ?', {
                    name: name,
                    CreatedOn: CurrentDate,
                    CreatedBy: req.session.user,
                    adress1: req.body.newAdress1,
                    description: req.body.description,
                    transaction_id: res_struct.idTransaction,
                    ClearingScreenOption: req.body.ClearingScreenOption,
                    TransactedwithoutCI: req.body.TransactedwithoutCI
                });

                if(req.body.ClearingScreenOption=='1'||req.body.TransactedwithoutCI=='1') {
                    resFunс = verify(req.session.user,req.body.newFirm,req.body.ClearingScreenOption,req.body.TransactedwithoutCI);
                    if (!resFunс){
                        res.send("Ethereum TestNET is unavailable, please try again in couple of minutes");
                        return;
                    }
                }

				res.send(res_struct.idTransaction);
			}
		});

	}
});

app.post('/selectAdressesCounterparties',function (req,res,next) {
    var adresses ='';
	var userTable = '';
    pool.pool.query('select bt.firms.name,'+
        '        bt.firms.adress1,'+
        '        bt.firms.description,'+
        '        bt.firms.transaction_id,'+
        '        bt.firms.CreatedOn,'+
        '        bt.firms.CreatedBy,'+
        '        innn.cleared,'+
        '        innn.transacted,'+
        '        innn.user'+
        '        from bt.firms '+
        '        left join '+
        ' 		 (select bt.log.cleared as cleared,'+
        ' 		 bt.log.transacted as transacted, '+
        ' 		 bt.log.user as user, '+
        '        bt.log.name as name '+
        '        from bt.log '+
        '        inner join'+
        '        (select bt.log.name as name, bt.log.user as user, max(bt.log.date) as date from bt.log'+
        '        where bt.log.user="'+req.session.user+'" and bt.log.name="'+req.body.name+'" group by bt.log.user, bt.log.name) as inn '+
        '        on (bt.log.name=inn.name) and (bt.log.user=inn.user) and (bt.log.date=inn.date)) as innn '+
        '        on (bt.firms.name=innn.name) where bt.firms.name="'+req.body.name+'"',function (err,rows,field) {
        if (err) throw err;
		var x=' ';
        var disabledClearing = 'false';
        var disabledTransacted = 'false';
        var y = ' ';
        var z = ' ';
		if(rows[0].cleared=='1'){
			x = 'checked/';
            disabledClearing = 'true';
		}
		if(rows[0].transacted=='1'){
			y = 'checked/';
            disabledTransacted = 'true';
		}
		var date = rows[0].CreatedOn;
        var newDate = '';
        if(date=='0000-00-00 00:00:00') {
            newDate = date;
        }else {
            newDate = date.toLocaleDateString() + ' ' + date.toLocaleTimeString()
        }
        if(rows.length) {
            adresses +=  '<div class="row">' +
                '<div class="col-md-4 h4">Counterparty\'s Name:</div>' +
                '<div class="col-md-8 h5" id="changing-adress">' + rows[0].name + '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-md-4 h4">Counterparty\'s Description:</div>' +
                '<div class="col-md-8 h5">' + rows[0].description + '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-md-4 h4">Registered Country:</div>' +
                '<div class="col-md-8 h5">' + rows[0].adress1 + '</div>' +
                '</div>' +
                // '<div class="row">'+
                // 	'<div class="col-md-4 h4">Counterparty Registration: </div>'+
                // 	'<div class="col-md-8 h5">'+' '+'</div>'+
                // '</div>'+
                '<div class="row">' +
                '<div class="col-md-4 h4">KYC Process </div>' +
                '<div class="col-md-8">' +
                '<div class="checkbox">' +
                '<label>' +
                '<input type="checkbox" id="ChekBoxClearedCounterparty" value="" ' + x + ' disabled class="h5" style="font-size: 33px"><span class="ttip"> Clearing Screen Option <em> by ticking this box you are sharing with the community on this platform (listed here) that you as a company have successfully ‘on-boarded this legal entity but you nor the company you represent can be held liable in any way for decisions others make based on this information</em></span>' +
                '</label><br>' +
                '<label>' +
                '<input type="checkbox" id="ChekBoxTransactedCounterparty" value="" ' + y + ' disabled class="h5"><span class="ttip"> Transacted without Credit Issues <em> by ticking this box you are sharing with the community on this platform (listed here) that you as a company have successfully completed a transaction of some form with this legal entity but you nor the company you represent can be held liable in any way for decisions others make based on this information</em></span>' +
                '</label><br>' +
                // '<label>'+
                //     '<input type="checkbox" value=""  disabled class="h5">Document Upload'+
                // '</label>'+
                '</div>' +
                '</div>' +
                '</div>' +
                // '<div class="row">'+
                // 	'<div class="col-md-4 h4">Counterparty Ledger: </div>'+
                // 	'<div class="col-md-8 h5">'+' '+'</div>'+
                // '</div>'+
                '<div class="row">' +
                '<div class="col-md-4 h4">Blockchain Record ID:</div>' +
                '<div class="col-md-8 h6" style="word-wrap: break-word"><a href="https://testnet.etherscan.io/tx/'+rows[0].transaction_id+'" target="_blank">' + rows[0].transaction_id + '</a></div>' +
                '</div>'+
                '<div class="row">' +
                '<div class="col-md-4 h4"></div>' +
                '<div class="col-md-8 h5" style="word-wrap: break-word">Created on:&nbsp;' +  newDate + '&nbsp;by&nbsp;'+'<span style="font-size: 14px;">'+  rows[0].CreatedBy +'</span></div>' +
                '</div>';
                // '<span class="h5 " >Created on: &nbsp;</span><span style="font-size: 14px;">'+  newDate +
                // '</span><span class="h5">&nbsp;by&nbsp;</span><span style="font-size: 14px;">'+  rows[0].CreatedBy +'</span>';
            var QueryText =
                'select bt.log.* from bt.log ' +
                'inner join ( ' +
                'SELECT ' +
                'bt.log.name as name, ' +
                'bt.log.user as user, ' +
                'max(bt.log.date) as date ' +
                'FROM bt.log ' +
                'group by ' +
                'bt.log.name, ' +
                'bt.log.user ) ' +
                'as inn ' +
                'on (bt.log.name = inn.name) & ' +
                '(bt.log.user = inn.user) & ' +
                '(bt.log.date = inn.date) where bt.log.name="' + req.body.name + '"';

            pool.pool.query(QueryText, function (err, rows, field) {
                if (err) throw err;
                var galCleared = '';
                var galTransacted = '';
                for (var j = 0; j < rows.length; j++) {
                    if (rows[j].cleared == '1') {
                        galCleared = '&#10003;';
                    } else {
                        galCleared = '';
                    }
                    if (rows[j].transacted == '1') {
                        galTransacted = '&#10003;';
                    } else {
                        galTransacted = '';
                    }
                    var newDate = '';
                    var date = rows[j].date;
                    if(date=='0000-00-00 00:00:00') {
                        newDate = date;
                    }else {
                        newDate = date.toLocaleDateString() + ' ' + date.toLocaleTimeString()
                    }
                    userTable += '<tr>' + '<td>' + rows[j].user + '</td>' + '<td>' + galCleared + '</td>' + '<td>' + galTransacted +
                        '</td>' + '<td class="h5">' + newDate + '</td>' + '<td class="h5"><a href="https://testnet.etherscan.io/tx/'+rows[j].transaction_id+'" target="_blank">' + rows[j].transaction_id + '</a></td>' + '</tr>';
                }
                res.send(adresses + '!' + userTable + '!' + disabledClearing + '!' + disabledTransacted);

            });
        }else {
            res.send('');
        }
    })
});

app.post('/selectCounterparty',function (req,res,next) {
	var table = '';
	var clearedCheck='';
	var transactedCheck='';
    pool.pool.query('select name from firms',function (err,rows,field) {
        if (err) throw err;
			for (var i = 0; i < rows.length; i++) {
				var name = rows[i].name;
				if (name.toLowerCase().indexOf(req.body.Counterparty.toLowerCase()) + 1) {
					table += '<tr onclick="selectFromTable(this)" class="colorContp"><td class="nameCounterparty" >' + rows[i].name + '</td></tr>';

				}
			}
			res.send(table);

    });

});

function verify(user,firm,cleared,transacted) {
        var CurrenDate = new Date();

        //insert record to the blockchain
        var param = user + "#" + firm + "#" + cleared + "#" + transacted;
        var res_struct = GetTransactionBlockChain(param);
        if (res_struct ==-1) {
           return false;
        }

        var userTable = '';
        var galCleared = '';
        var galTransacted = '';
        if (user) {
            pool.pool.query('insert into log set ?', {
                date: CurrenDate,
                name: firm,
                user: user,
                Cleared: cleared,
                Transacted: transacted,
                hash: res_struct.hash,
                transaction_id: res_struct.idTransaction
            });
        }
    return true;
}

app.post('/VerifyCounterparty', function (req, res, next) {
    var CurrenDate = new Date();
    var userTable = '';
    var galCleared = '';
    var galTransacted = '';
    if (req.session.user) {
        //insert record to the blockchain
        var param = req.body.user + "#" + req.body.firm + "#" + req.body.ChekBoxCleared + "#" + req.body.ChekBoxTransacted;
        var res_struct = GetTransactionBlockChain(param);
        if (res_struct ==-1) {
            res.send("Ethereum TestNET is unavailable, please try again in couple of minutes");
            return;
        }

        if(req.body.ChekBoxCleared=='1'||req.body.ChekBoxTransacted=='1'){
        pool.pool.query('insert into log set ?', {
            date: CurrenDate,
            name: req.body.firm,
            user: req.session.user,
            Cleared: req.body.ChekBoxCleared,
            Transacted: req.body.ChekBoxTransacted,
            hash: res_struct.hash,
            transaction_id: res_struct.idTransaction
        }, function (err, rows, field) {
            var f = 1;

            var QueryText =
                'select bt.log.* from bt.log ' +
                'inner join ( ' +
                'SELECT ' +
                'bt.log.name as name, ' +
                'bt.log.user as user, ' +
                'max(bt.log.date) as date ' +
                'FROM bt.log ' +
                'group by ' +
                'bt.log.name, ' +
                'bt.log.user ) ' +
                'as inn ' +
                'on (bt.log.name = inn.name) & ' +
                '(bt.log.user = inn.user) & ' +
                '(bt.log.date = inn.date) where bt.log.name="' + req.body.firm + '"';

            pool.pool.query(QueryText, function (err, rows, field) {
                if (err) throw err;
                for (var j = 0; j < rows.length; j++) {
                    if (rows[j].cleared == '1') {
                        galCleared = '&#10003;';
                    } else {
                        galCleared = ' ';
                    }
                    if (rows[j].transacted == '1') {
                        galTransacted = '&#10003;';
                    } else {
                        galTransacted = ' ';
                    }
                    var date = rows[j].date;
                    if(date=='0000-00-00 00:00:00') {
                        newDate = date;
                    }else {
                        newDate = date.toLocaleDateString() + ' ' + date.toLocaleTimeString()
                    }
                    userTable += '<tr>' + '<td>' + rows[j].user + '</td>' + '<td>' + galCleared + '</td>' + '<td>' + galTransacted +
                        '</td>' + '<td class="h5">' + newDate + '</td>' + '<td class="h5"><a href="https://testnet.etherscan.io/tx/'+rows[j].transaction_id+'" target="_blank">' + rows[j].transaction_id + '</a></td>' + '</tr>';
                }
                res.send(userTable);
            });
        });
    }else{
        res.send('');
        }
    } else {
        res.send('no');
    }

});


app.use(function(err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: {}
	});
});


module.exports = app;

