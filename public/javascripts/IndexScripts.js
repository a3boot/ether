function addNewFirm() {
    if(document.getElementById('addNewNameCounterparty').value==''){
        document.getElementById('takeAnotherFirm').innerHTML = 'You have to fill in Counterparty\'s name';
    }else {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                if (xhr.responseText == 'The Firm already exists') {
                    document.getElementById('takeAnotherFirm').innerHTML = 'This counterparty\'s name already exist';
                } else if (xhr.responseText == 'You should login first') {
                    document.getElementById('takeAnotherFirm').innerHTML = 'You have login first';
                } else {
                    hideButton();
                    document.getElementById('takeAnotherFirm').innerHTML = '<a href="https://testnet.etherscan.io/tx/' + xhr.responseText + '" target="_blank">' + xhr.responseText + '</a>';
                }
            }


            if (xhr.readyState == 4 && xhr.status != 200) {
                document.getElementById('takeAnotherFirm').innerHTML = 'Ethereum TestNET is unavailable, please try again in couple of minutes';
            }
        };
        xhr.open('POST', '/addNewFirm', true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
        var data = 'newFirm=' + document.getElementById('addNewNameCounterparty').value +
            '&newAdress1=' + document.getElementById('addNewAdress1Counterparty').value +
            '&description=' + document.getElementById('addNewDescriptionCounterparty').value +
            '&ClearingScreenOption=' + (document.getElementById('ClearingScreenOption').checked ? 1 : 0) +
            '&TransactedwithoutCI=' + (document.getElementById('TransactedwithoutCI').checked ? 1 : 0);
        xhr.send(data);
    }

}

function selectAdressOnAddPage(){
    window.location.href='/content';
}

function hideButton() {
    if(document.getElementById('addNewNameCounterparty').value==''){

    }else {
        var keyOne = document.getElementById("key1");
        var keyTwo = document.getElementById("key2");

        var elemIn = document.getElementsByClassName("buttons");
        var chB = document.getElementsByClassName("chBox");
        for (var i = 0; i <= elemIn.length - 1; i++) {
            var el = elemIn[i];
            el.setAttribute("readonly", "readonly");
            keyOne.style.display = "none";
            keyTwo.style.display = "inline";
        }
        for (var j = 0; j < chB.length; j++) {
            var box = chB[j];
            box.setAttribute("disabled", "true");
        }
    }
}

function unhideButtons(){
    var keyOne = document.getElementById("key1");
    var keyTwo = document.getElementById("key2");
    var elemIn = document.getElementsByClassName("buttons");
    var chB = document.getElementsByClassName("chBox");
    for (var i = 0; i <= elemIn.length - 1; i++) {
        var el = elemIn[i];
        el.value = '';
        el.removeAttribute("readonly");
        keyTwo.style.display = "none";
        keyOne.style.display = "inline";
        document.getElementById('takeAnotherFirm').innerHTML ='';
    }
    for (var j = 0; j < chB.length; j++) {
        var box = chB[j];
        box.removeAttribute("disabled");
        box.checked = '';
    }

}

function select() {
    var divEl = document.getElementById('height1');
    var val = document.getElementsByName('scrollToThere');
    for(var i=0;i<val.length;i++) {
        divEl.scrollTop = val[i].offsetTop;
        val[i].click();
    }

}

select();
function selectFromTable(e){
   var el= document.getElementsByClassName("colorContp");
    for(var i=0;i<el.length;i++){
        el[i].style.backgroundColor='#eee';
    }
e.style.backgroundColor='lightblue';

                 var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                     if (xhr.readyState == 4 && xhr.status == 200) {
                        if(xhr.responseText) {
                            var y = xhr.responseText.split('!');
                            document.getElementById('tableAdr').style.opacity = 1;
                            document.getElementById('showAdress').innerHTML = y[0];
                            document.getElementById('usersTable').innerHTML = y[1];
                            document.getElementById('ChekBoxCleared').removeAttribute("disabled");
                            document.getElementById('ChekBoxTransacted').removeAttribute("disabled");
                            document.getElementById('ChekBoxCleared').checked = 0;
                            document.getElementById('ChekBoxTransacted').checked = 0;
                            document.getElementById('verifyButton').style.opacity=0.4;
                            if(y[2]=='true') {
                                document.getElementById('ChekBoxCleared').checked = 1;
                                document.getElementById('ChekBoxCleared').disabled = true;
                                document.getElementById('verifyButton').disabled = true;
                                document.getElementById('verifyButton').style.opacity=0.4;
                            }
                            if(y[3]=='true') {
                                document.getElementById('ChekBoxTransacted').checked = 1;
                                document.getElementById('ChekBoxTransacted').disabled = true;
                                document.getElementById('verifyButton').disabled = true;
                                document.getElementById('verifyButton').style.opacity=0.4;
                            }
                            if(y[2]=='true'&&y[3]=='true'){
                                document.getElementById('verifyButton').disabled = true;
                                document.getElementById('verifyButton').style.opacity=0.4;
                            }
                     }
                    }
                     if (xhr.readyState == 4 && xhr.status != 200) {
                         document.getElementById('etherErrMessage').innerHTML='Ethereum TestNET is unavailable, please try again in couple of minutes';
                    }
                };
                 xhr.open('POST', '/selectAdressesCounterparties', true);
                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                var data = 'name='+e.innerHTML.split('>')[1].split('<')[0];
                xhr.send(data);


}

function undisableButton() {
    if(document.getElementById('ChekBoxClearedCounterparty').checked==0&&document.getElementById('ChekBoxCleared').checked==1||
        document.getElementById('ChekBoxTransactedCounterparty').checked==0&&document.getElementById('ChekBoxTransacted').checked==1){
        document.getElementById('verifyButton').removeAttribute("disabled");
        document.getElementById('verifyButton').style.opacity=1;
    }else{
        document.getElementById('verifyButton').disabled = true;
        document.getElementById('verifyButton').style.opacity=0.4;
    }
}

function selectCounterparty() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            if(xhr.responseText==''){
                document.getElementById('showCounterparty').innerHTML = '';
            }else{
                document.getElementById('showCounterparty').innerHTML = xhr.responseText;
                document.getElementById('showAdress').innerHTML ='<div class="row">'+
                    '                                    <div class="col-md-4 h4">Counterparty\'s Name:</div>'+
                    '                                    <div class="col-md-8 h5"></div>'+
                    '                                </div>'+
                    '                                <div class="row">'+
                    '                                    <div class="col-md-4 h4">Counterparty\'s Description:</div>'+
                    '                                    <div class="col-md-8 h5"></div>'+
                    '                                </div>'+
                    '                                <div class="row">'+
                    '                                    <div class="col-md-4 h4">Registered Country:</div>'+
                    '                                    <div class="col-md-8 h5"></div>'+
                    '                                </div>'+
                    '                                <!--                                   <div class="row">'+
                    '                                                                       <div class="col-md-4 h4">Counterparty Registration: </div>'+
                    '                                                                       <div class="col-md-8 h5"></div>'+
                    '                                                                       </div>-->'+
                    '                                <div class="row">'+
                    '                                    <div class="col-md-4 h4">KYC Process </div>'+
                    '                                    <div class="col-md-8">'+
                    '                                        <div class="checkbox">'+
                    '                                            <label>'+
                    '                                                <input type="checkbox" value="" disabled class="h5" style="font-size: 33px"><span class="h5 ttip"> Clearing Screen Option <em> by ticking this box you are sharing with the community on this platform (listed here) that you as a company have successfully ?on-boarded this legal entity but you nor the company you represent can be held liable in any way for decisions others make based on this information</em></span>'+
                    '                                            </label><br>'+
                    '                                            <label>'+
                    '                                                <input type="checkbox" value="" disabled class="h5"><span class="h5 ttip"> Transacted without Credit Issues <em>by ticking this box you are sharing with the community on this platform (listed here) that you as a company have successfully completed a transaction of some form with this legal entity but you nor the company you represent can be held liable in any way for decisions others make based on this information</em></span>                                            </label><br>'+
                    '                                            <!--                                         <label>'+
                    '                                                                                         <input type="checkbox" value="" disabled class="h5">Document Upload'+
                    '                                                                                         </label>-->'+
                    '                                        </div>'+
                    '                                    </div>'+
                    '                                </div>'+
                    '                                <!--                            <div class="row">'+
                    '                                                                <div class="col-md-4 h4">Counterparty Ledger: </div>'+
                    '                                                                <div class="col-md-8 h5"></div>'+
                    '                                                                </div>-->'+
                    '                                <div class="row">'+
                    '                                    <div class="col-md-4 h4">Blockchain Record ID:</div>'+
                    '                                    <div class="col-md-8 h6" style="word-wrap: break-word"></div>'+
                    '                                </div>'+ '<div class="row">' +  '<div class="col-md-4 h4"></div>' +  '<div class="col-md-8 h5" style="word-wrap: break-word">Created on:&nbsp;&nbsp;by&nbsp;'+
                                                '</div>' +
                                                '</div>';
                document.getElementById('ChekBoxCleared').checked = 0;
                document.getElementById('ChekBoxTransacted').checked = 0;
                document.getElementById('ChekBoxCleared').disabled = true;
                document.getElementById('ChekBoxTransacted').disabled = true;
                document.getElementById('verifyButton').disabled = true;
                document.getElementById('tableAdr').style.opacity = 0.5;
            }
        }

        if (xhr.readyState == 4 && xhr.status != 200) {
            document.getElementById('etherErrMessage').innerHTML='Ethereum TestNET is unavailable, please try again in couple of minutes';
        }
    };
    xhr.open('POST', '/selectCounterparty', true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
    var data = 'Counterparty='+document.getElementById('counterparties').value;
    xhr.send(data);
    return false;
}


function VerifyCounterparty() {
    if(document.getElementById('changing-adress')==undefined){
        document.getElementById('needLogin').innerHTML ="You must select a Counterparty";
        return;
    }
    if (document.getElementById('changing-adress').value == "") {
        document.getElementById('needLogin').innerHTML ="You must select a Counterparty";
        return;
    }
    var xhr = new XMLHttpRequest();
    //
    xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                if(xhr.responseText=='no'){
                    document.getElementById('needLogin').innerHTML = 'You should login first';
                }else if(xhr.responseText==''){
                	
                }else {
                    document.getElementById('usersTable').innerHTML = xhr.responseText;
                    if(document.getElementById('ChekBoxCleared').checked==1) {
                        document.getElementById('ChekBoxClearedCounterparty').checked = 1;
                        document.getElementById('ChekBoxCleared').disabled = true;
                        document.getElementById('verifyButton').disabled = true;
                        document.getElementById('verifyButton').style.opacity=0.4;
                    }
                    if(document.getElementById('ChekBoxTransacted').checked==1) {
                        document.getElementById('ChekBoxTransactedCounterparty').checked = 1;
                        document.getElementById('ChekBoxTransacted').disabled = true;
                        document.getElementById('verifyButton').disabled = true;
                        document.getElementById('verifyButton').style.opacity=0.4;
                    }
                    if(document.getElementById('ChekBoxCleared').checked==1&&document.getElementById('ChekBoxTransacted').checked==1){
                        document.getElementById('verifyButton').disabled = true;
                        document.getElementById('verifyButton').style.opacity=0.4;
                    }
                }
            }else if (xhr.readyState == 4 && xhr.status != 200) {
                document.getElementById('etherErrMessage').innerHTML='Ethereum TestNET is unavailable, please try again in couple of minutes';
            }
    };
    
    xhr.open('POST', '/VerifyCounterparty', true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8");

    var DataStr =  'firm='+document.getElementById('changing-adress').innerHTML+
                   '&ChekBoxCleared=' + (document.getElementById('ChekBoxCleared').checked ? 1:0)+
                   '&ChekBoxTransacted=' +  (document.getElementById('ChekBoxTransacted').checked  ? 1:0) ;



    xhr.send(DataStr);
}

function UserLogin(form) {
    if (document.getElementById('userLogin').value == "" ){
        alert("You must select a user");
        return;
    }
        var xhr = new XMLHttpRequest();

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                if(xhr.responseText=='') {
                    window.location.href='/content';
                 }else{
                    document.getElementById('takeAnotherLogin').innerHTML = "Wrong login or password";
                }
             }else if (xhr.readyState == 4 && xhr.status != 200) {
                 document.getElementById('etherErrMessage').innerHTML='Ethereum TestNET is unavailable, please try again in couple of minutes';
             }
        };
        xhr.open('POST', '/login', true);
        xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
        var firm = 'user=' + document.getElementById('userLogin').value+
                   '&password='+document.getElementById('userPassword').value;
        xhr.send(firm);
    return false;
}

function logOut() {
    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            window.location.href='/';
            if(xhr.responseText=='') {
                document.getElementById('logoutButtonId').innerHTML = '';
                document.getElementById('loginForm').innerHTML = '<form>' +
                    '<label>' + 'Login:' + '<br>' + '<input type="text" id="userLogin">' + '</label>' + '<br>' +
                    '<label>' + 'Password:' + '<br>' + '<input type="password" id="userPassword">' + '</label>' + '<br>' +
                    '<input type="button" name="" value="login" class="login_form-button" onclick="UserLogin()">' +
                    '</form>';
            }
        }else if (xhr.readyState == 4 && xhr.status != 200) {

            document.getElementById('etherErrMessage').innerHTML='Ethereum TestNET is unavailable, please try again in couple of minutes';

        }
    };
    xhr.open('POST', '/logout', true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8");

    xhr.send('delete Sess');
}

function hoverForButton() {
    document.getElementById('button_disclaimer').style.color = '#333';
}

function unhoverForButton() {
    document.getElementById('button_disclaimer').style.color = 'red';
}
