var express = require('express');
var router = express.Router();
var sql = require('mysql');
var pool = require('../bin/conn/index');
var url = require('url');

router.get('/', function(req, res, next) {
    if(!req.session.user){
        res.redirect('/');
    }else {

        var firmsTable = '';

        function getFirms() {
            pool.pool.query('select name from firms', function (err, rows, field) {
                if (err) throw err;
                for (var i = 0; i < rows.length; i++) {
                    if(rows[i].name==req.session.currentCounterparty){
                        firmsTable += '<tr onclick="selectFromTable(this)" class="colorContp" style="background-color: lightblue;" name="scrollToThere">' +
                            '<td class="nameCounterparty">' + rows[i].name + '</td>' + '</tr>';
                    }else{
                        if(req.session.firstLog=='ok'){
                            firmsTable += '<tr onclick="selectFromTable(this)" class="colorContp" style="background-color: lightblue;" name="scrollToThere">' +
                                '<td class="nameCounterparty">' + rows[0].name + '</td>' + '</tr>';
                            delete req.session.firstLog;
                        }else {
                            firmsTable += '<tr onclick="selectFromTable(this)" class="colorContp">' +
                                '<td class="nameCounterparty" >' + rows[i].name + '</td>' + '</tr>';
                        }
                    }
                }
                renderContent();
            });
        }

        var logoutButton = '';

        function renderContent() {
            delete req.session.currentCounterparty;
            if (req.session.user) {
                logoutButton =' <input type="button" name="" value="Logout" onclick="logOut()" ><br>'+'<div class="log_text">'+
                    'Logged in as <span class="log_user">'+req.session.user+'</span></div>';
            }
            res.render('content', {title: 'Express', firmsTable: firmsTable, logoutButton: logoutButton});
        }

        getFirms();
    }
});

module.exports = router;