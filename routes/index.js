var express = require('express');
var router = express.Router();
var fs = require('fs');
var mysql = require('mysql');
var pool = require('../bin/conn/index');

/* GET home page. */


router.get('/', function(req, res, next) {
    if(req.session.user){
        res.redirect('/content');
    }else {
        var loginMenu = '';
        if (req.session.user) {
            loginMenu = '<input type="button" value="logout" onclick="logOut()">';
        } else {
            loginMenu = '<form  onsubmit="return UserLogin(this)">' +
                '<label>Login:<br><input type="text" id="userLogin" autofocus="true"></label><br>' +
                '<label>Password:<br><input type="password" id="userPassword"></label><br>' +
                '<input type="submit" name="" value="login" class="login_form-button">' +
                '</form>';
        }
        res.render('index', {title: 'Express', loginMenu: loginMenu});
    }
});


module.exports = router;
