var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    if(!req.session.user){
        res.redirect('/');
    }else {
        var logoutButton = '';
        if (req.session.user) {
            logoutButton =' <input type="button" name="" value="Logout" onclick="logOut()" ><br>'+'<div class="log_text">'+
                'Logged in as <span class="log_user">'+req.session.user+'</span></div>';
        }
        res.render('add', {title: 'Express', logoutButton: logoutButton});
    }
});

module.exports = router;
