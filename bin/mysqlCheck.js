var sql = require("mysql");
var config = require('../config');
//-------------------------------------------
//Create connection with db without database define for testing (must be createConnection rather createPool)
//-------------------------------------------
var pool = sql.createConnection({
    connectionLimit : config.connectionLimit,
    host : config.host,
    user : config.user,
    password : config.password
});
console.log(' test database "bt"  ');
function showDb() {
    pool.query("show databases like 'bt' ",function (err, rows) {
        if (err) throw err;
        if (rows.length == 0) {
            console.log(' database "bt" NOT exist ');
            creatDb();
        } else {
            console.log(' database "bt" exist /n');

            pool.query('use bt',function () {
                pool.query('show columns from firms like "CreatedOn"', function (err, rows) {
                    if (err) throw err;
                    if (rows.length == 0) {
                        console.log('column "createdOn" NOT exist ');
                        pool.query('alter table firms add CreatedOn datetime not NULL, add CreatedBy varchar(100) not NULL', function (err) {
                            if (err) throw err;
                            process.exit();
                        });
                    } else {
                        process.exit();
                    }
                });
            });

        }
    });
}

function creatDb() {
    pool.query('CREATE DATABASE bt CHARACTER SET utf8',function (err) {
        if (err) throw err;
        usingDb();
    });

}

function usingDb() {
    pool.query('USE bt ',function (err) {
        if (err) throw err;
        creatTableFirms();
        creatTablelog();
        creatTableUsers();
    });
}

function creatTableFirms() {
    pool.query(

        "CREATE TABLE firms (name varchar(100) NOT NULL, adress1 varchar(100) DEFAULT NULL,description varchar(100) DEFAULT NULL," +
        " transaction_id varchar(70) DEFAULT NULL, VitolSA varchar(100) DEFAULT NULL, ClearingScreenOption boolean, TransactedwithoutCI boolean," +
        " DocumenUpload boolean, ViewLedger varchar(100) DEFAULT NULL, CreatedOn datetime NOT NULL, CreatedBy varchar(100) NOT NULL)",

        function (err) {
            if (err) throw err;
        });
}

function creatTablelog() {
    pool.query(
        "CREATE TABLE log (date datetime NOT NULL,user varchar(100) DEFAULT NULL,name varchar(100) DEFAULT NULL,cleared int(11) DEFAULT NULL,"+
        "transacted int(11) DEFAULT NULL,"+
        "hash varchar(70) DEFAULT NULL,"+
        "transaction_id varchar(70) DEFAULT NULL)",
        function (err) {
            if (err) throw err;
        });
}
function creatTableUsers() {
    pool.query(
        "CREATE TABLE users (id INT(11) NOT NULL AUTO_INCREMENT, name varchar(100) DEFAULT NULL, password varchar(20) DEFAULT NULL,PRIMARY KEY (id))",
        function (err) {
            if (err) throw err;
            insertIntoUsers();
        });

}

function insertIntoUsers() {
    pool.query(
        "INSERT INTO users VALUES (1,'BP','123'),(2,'Shell','123'),(3,'Statoil','123'),(4,'Total','123'),(5,'Helm','123'),(6,'Vinmar','123'),(7,'Interchem','123')",
        function (err) {
            if (err) throw err;
            process.exit();
        });
}

showDb();

//-------------------------------------------
//End connection with db
//-------------------------------------------