var express = require('express');
var sql = require("mysql");
var config = require('../../config');
//-------------------------------------------
//Create connection with db
//-------------------------------------------
var pool = sql.createPool({
    connectionLimit : config.connectionLimit,
    host : config.host,
    user : config.user,
    password : config.password,
    database : config.database
});
module.exports.pool = pool;
//-------------------------------------------
//End connection with db
//-------------------------------------------